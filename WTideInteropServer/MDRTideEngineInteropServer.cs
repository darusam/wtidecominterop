﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MDRTELib;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;
using WTideInteropClient;
using System.Security.AccessControl;

namespace WTideInteropServer
{
    public class MDRTideEngineInteropServer
    {
        private static NamedPipeServerStream _pipe;
        public static void Main(string[] args)
        {
            if(args.Length < 1)
            {
                return;
            }

            string pipeName = args[0];
            
            PipeSecurity ps = new PipeSecurity();
            ps.AddAccessRule(new PipeAccessRule("Users", PipeAccessRights.ReadWrite | PipeAccessRights.CreateNewInstance, AccessControlType.Allow));
            ps.AddAccessRule(new PipeAccessRule("CREATOR OWNER", PipeAccessRights.FullControl, AccessControlType.Allow));
            ps.AddAccessRule(new PipeAccessRule("SYSTEM", PipeAccessRights.FullControl, AccessControlType.Allow));
            //ps.AddAccessRule(pa);

            ps.AddAccessRule(new PipeAccessRule("Users", PipeAccessRights.FullControl, System.Security.AccessControl.AccessControlType.Allow));
            ps.AddAccessRule(new PipeAccessRule("Everyone", PipeAccessRights.ReadWrite, System.Security.AccessControl.AccessControlType.Allow));
            //ps.AddAccessRule(par);
            
            using(NamedPipeServerStream pipe = new NamedPipeServerStream(pipeName, PipeDirection.InOut, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous, 4096, 4096, ps))
            {
                _pipe = pipe;

                IAsyncResult waitResult = pipe.BeginWaitForConnection(HandleWaitConnection, null);

                Console.WriteLine(string.Format("--BEGIN PIPE NAME--\n{0}\n--END PIPE NAME--\n", pipeName.ToString()));

                WaitHandle waitHandle = waitResult.AsyncWaitHandle;
                waitHandle.WaitOne();

                

                System.Runtime.Serialization.IFormatter formatter = new BinaryFormatter();
                TideEngine engine = new TideEngine();
                engine.Open();

                CallData callData;
                while ((callData = (CallData)formatter.Deserialize(pipe)).Status != KeepAliveStatus.Close)
                {
                    HandleCallData(callData, engine, formatter, pipe);
                }
                

            }
        }

        private static void HandleWaitConnection(IAsyncResult result)
        {
            _pipe.EndWaitForConnection(result);
        }
        public static object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }


        private static void HandleCallData(CallData callData, TideEngine engine, System.Runtime.Serialization.IFormatter formatter, Stream pipeStream)
        {
            try
            {
                Type type = typeof(InteropResponse<>).MakeGenericType(callData.Delegate);
                IInteropResponse toReturn = null;
                switch (callData.ProcedureName)
                {
                    case "Test":
                        toReturn = new InteropResponse<string>("CONNECTED");
                        break;
                    case "Create":
                        toReturn = new InteropResponse<string>(string.Format("{0}", Create(engine)));
                        break;
                    case "SetProperty":
                        try
                        {
                            SetProperty(engine, (string)callData.Parameters[0], callData.Parameters[1]);
                            toReturn = (IInteropResponse)Activator.CreateInstance(type, new object[] { GetDefault(callData.Delegate) });
                        }
                        catch(Exception ex)
                        {
                            toReturn = new InteropException(
                                string.Format("Error setting property {0}", callData.Parameters[0]),
                                ex
                             );
                        }
                        break;
                    case "GetProperty":
                        toReturn = (IInteropResponse) Activator.CreateInstance(type,  new object[] { GetProperty(engine, (string)callData.Parameters[0]) });
                        break;
                    case "CreateIndexSubset":
                        try
                        {
                            toReturn = (IInteropResponse)Activator.CreateInstance(type,  new object[] { CreateIndexSubset(engine, callData.Parameters[0], callData.Parameters[1], callData.Parameters[2]) });
                        }
                        catch(Exception ex)
                        {
                            toReturn = new InteropException(
                                string.Format("Error Creating Index Subset with lat {0}, lng {1} and radius {2} ", callData.Parameters[0], callData.Parameters[1], callData.Parameters[2]),
                                ex
                             );
                        }
                        break;
                    case "DestroyIndexSubset":
                        try
                        {
                            DestroyIndexSubset(engine);
                            toReturn = new InteropResponse<object>(null);
                        }
                        catch(Exception ex)
                        {
                            toReturn = new InteropException("Error destroying index subset", ex);
                        }
                        break;
                    case "GetSettings":
                        toReturn = (IInteropResponse)Activator.CreateInstance(type,  new object[] { GetSetting(engine, null) });
                        break;
                    case "SetSetting":
                        try
                        {
                            SetSetting(engine, (string)callData.Parameters[0], callData.Parameters[1]);
                            toReturn = (IInteropResponse)Activator.CreateInstance(type, new object[] { GetDefault(callData.Delegate) });
                        }
                        catch(Exception ex)
                        {
                            toReturn = new InteropException(
                                string.Format("Error Setting MDR Tide Engine Setting {0}", callData.Parameters[0]),
                                ex
                             );
                        }
                        break;
                    case "GetSetting":
                        toReturn = (IInteropResponse)Activator.CreateInstance(type, new object[] { GetSetting(engine, (string)callData.Parameters[0]) });
                        break;
                    case "Find":
                        toReturn = new InteropResponse<string>(string.Format("{0}", Find(engine, callData.Parameters[0])));
                        break;
                    case "FindClosest":
                        object distance;
                        toReturn =  new InteropResponse<string>(string.Format("{0}", FindClosest(engine, callData.Parameters[0], callData.Parameters[1], out distance)));
                        break;
                }
                
                formatter.Serialize(pipeStream, toReturn);
            }
            catch(Exception e)
            {
                formatter.Serialize(pipeStream, new InteropException("An error occured while calling MDR Tide Engine", e));
            }
        }
        
        private static void SetProperty(TideEngine te, string propName, object value)
        {
            switch(propName)
            {
                case "ChartType":
                    te.ChartType = value;
                    //te.set_ChartType(value);
                    break;
                case "EndTime":
                    te.EndTime = value;
                    //te.set_EndTime(value);
                    break;
                case "FileName":
                    te.FileName = value.ToString();
                    //te.set_FileName(value);
                    break;
                case "LocationName":
                    te.set_LocationName(value as string);
                    //te.set_LocationName(value);
                    break;
                case "StartTime":
                    te.StartTime = value;
                    //te.set_StartTime(value);
                    break;
                default:
                    throw new ArgumentException("Argument supplied is not supported or is Write-Only!");
            }
        }

        private static object GetProperty(TideEngine te, string propName)
        {
            switch(propName)
            {
                case "ChartType":
                    return te.ChartType;
                    //return te.get_ChartType();
                case "Count":
                    return te.Count;
                    //return te.get_Count();
                case "ErrorMessage":
                    return te.ErrorMessage;
                    //return te.get_ErrorMessage();
                case "Lines":
                    return te.Lines;
                    //return te.get_Lines();
                case "LocationName":
                    return te.get_LocationName();
                case "StationCount":
                    return te.StationCount;
                    //return te.get_StationCount();
                case "StationHarmonicsFileName":
                    return te.StationHarmonicsFileName;
                    //return te.get_StationHarmonicsFileName();
                case "StationIsCurrent":
                    return te.StationIsCurrent;
                    //return te.get_StationIsCurrent();
                case "StationIsHydraulicCurrent":
                    return te.StationIsHydraulicCurrent;
                    //return te.get_StationIsHydraulicCurrent();
                case "StationIsReferenceStation":
                    return te.StationIsReferenceStation;
                    //return te.get_StationIsReferenceStation();
                case "StationLat":
                    return te.StationLat;
                    //return te.get_StationLat();
                case "StationLng":
                    return te.StationLng;
                    //return te.get_StationLng();
                case "StationNote":
                    return te.StationNote;
                    //return te.get_StationNote();
                case "StationPedigree":
                    return te.StationPedigree;
                    //return te.get_StationPedigree();
                case "StationReferenceStationName":
                    return te.StationReferenceStationName;
                    //return te.get_StationReferenceStationName();
                case "StationTimeZone":
                    return te.StationTimeZone;
                    //return te.get_StationTimeZone();
                case "StationUnitOfMeasure":
                    return te.StationUnitOfMeasure;
                    //return te.get_StationUnitOfMeasure();
                default:
                    throw new ArgumentException("Argument supplied is not supported or is Write-Only!");
            }
        }

        private static object Create(TideEngine te)
        {
            return te.Create();
        }

        private static object CreateIndexSubset(TideEngine te, object lat, object lng, object radius)
        {
            return te.CreateIndexSubset(lat, lng, radius);
        }

        private static void DestroyIndexSubset(TideEngine te)
        {
            te.DestroyIndexSubset();
        }

        private static object Find(TideEngine te, object index)
        {
            object lat, lng;
            return Find(te, index, out lat, out lng);
        }

        private static object Find(TideEngine te, object index, out object lat, out object lng)
        {
            try
            {
                return te.Find(index, out lat, out lng);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                lat = null;
                lng = null;
                return e;
            }
        }

        private static object FindClosest(TideEngine te, object lat, object lng, out object distance)
        {
            return te.FindClosest(lat, lng, out distance);
        }

        private static void SetSetting(TideEngine te, string settingName, object value)
        {
            switch (settingName)
            {
                case "DateFormat":
                    te.Settings.DateFormat = value;
                    break;
                case "RawDataStep":
                    te.Settings.RawDataStep = value;
                    break;
                case "TimeFormat":
                    te.Settings.TimeFormat = value;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private static object GetSetting(TideEngine te, string settingName)
        {
            if(string.IsNullOrWhiteSpace(settingName))
            {
                return te.Settings;
            }
            switch(settingName)
            {
                case "DateFormat":
                    return te.Settings.DateFormat;
                case "RawDataStep":
                    return te.Settings.RawDataStep;
                case "TimeFormat":
                    return te.Settings.TimeFormat;
                default:
                    throw new NotImplementedException();
            }
            throw new NotImplementedException();

        }
    }
}
