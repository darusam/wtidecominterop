﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WTideInteropClient
{
    public class MDRTideEngineInteropClient : IDisposable
    {

        private bool _disposed;
        protected NamedPipeClientStream _client = null;
        protected Process _serverProcess = null;

        protected string _pipeName = null;
        protected MDRTideEngineSettings _settings = null;

        public DataReceivedEventHandler ServerOutputDataRecieved;
        public DataReceivedEventHandler ServerErrorDataRecieved;
        public DataReceivedEventHandler OutputDataRecieved;
        public DataReceivedEventHandler ErrorDataRecieved;


        public MDRTideEngineInteropClient(DataReceivedEventHandler serverStdOut = null, 
                                            DataReceivedEventHandler serverStdErr = null, 
                                            DataReceivedEventHandler stdOut = null, 
                                            DataReceivedEventHandler stdErr = null)
        {
            _pipeName = Guid.NewGuid().ToString();
            _settings = new MDRTideEngineSettings(this);


            ServerErrorDataRecieved = serverStdErr;
            ErrorDataRecieved = stdErr;

            
            ManualResetEvent evt = new ManualResetEvent(false);

            _serverProcess = new Process();
            _serverProcess.StartInfo.FileName = "WTideInteropServer.exe";
            _serverProcess.StartInfo.Arguments = _pipeName;
            _serverProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            _serverProcess.StartInfo.CreateNoWindow = true;
            _serverProcess.StartInfo.UseShellExecute = false;
            _serverProcess.StartInfo.RedirectStandardError = true;
            _serverProcess.StartInfo.RedirectStandardOutput = true;

            bool isListen = false;
            _serverProcess.OutputDataReceived += new DataReceivedEventHandler((sender, args) =>
            {
                if(string.IsNullOrWhiteSpace(args.Data))
                {
                    return;
                }
                if(isListen)
                {
                    if(args.Data.IndexOf(_pipeName) >= 0)
                    {
                        evt.Set();
                        isListen = false;
                    }
                }
                if(args.Data.IndexOf("--BEGIN PIPE NAME--") >= 0)
                {
                    isListen = true;
                }
                
            });
            _serverProcess.ErrorDataReceived += OnServerErrorRecieved;
            
            _serverProcess.Start();

            _serverProcess.BeginOutputReadLine();
            _serverProcess.BeginErrorReadLine();
            _client = new NamedPipeClientStream(".", _pipeName, PipeDirection.InOut);

            evt.WaitOne();
            _client.Connect();
            

            _client.ReadMode = PipeTransmissionMode.Message;
            
        }

        /// <summary>
        /// Executes a call to a library.
        /// </summary>
        /// <typeparam name="T">Delegate Type to call.</typeparam>
        /// <param name="library">Name of the library to load.</param>
        /// <param name="function">Name of the function to call.</param>
        /// <param name="args">Array of args to pass to the function.</param>
        /// <returns>Result object returned by the library.</returns>
        /// <exception cref="Exception">This Method will rethrow all exceptions thrown by the wrapper.</exception>
        public IInteropResponse<T> Invoke<T>(string library, string function, object[] args)// where T : class
        {
            if (_disposed)
                throw new ObjectDisposedException("MDRTideEngineInteropClient");

            var info = new CallData
            {
                Library = library,
                ProcedureName = function,
                Parameters = args,
                Delegate = typeof(T),
            };
            var formatter = new BinaryFormatter();
            // Write request to server
            formatter.Serialize(_client, info);

            // Receive result from server
            object result = formatter.Deserialize(_client);

            var exception = result as Exception;
            if (exception != null)
            {
                throw exception;
            }

            return (IInteropResponse<T>) result;
        }

        /// <summary>
        /// Gracefully close connection to server
        /// </summary>
        protected virtual void Close()
        {
            var info = new CallData { Status = KeepAliveStatus.Close };
            var formatter = new BinaryFormatter();
            formatter.Serialize(_client, info);

            if (_client.IsConnected)
            {
                _client.Close();
            }

        }

        #region IDisposable
        ~MDRTideEngineInteropClient()
        {
            Dispose();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                Close();
                _client.Dispose();
            }

            // Free any unmanaged objects here.
            _disposed = true;
        }

        #endregion

        protected void OnServerErrorRecieved(object sender, DataReceivedEventArgs args)
        {
            Console.WriteLine(args.Data);
            if(ServerErrorDataRecieved != null)
            {
                ServerErrorDataRecieved(sender, args);
            }
        }


        protected void OnErrorRecieved(object sender, DataReceivedEventArgs args)
        {
            if (ErrorDataRecieved != null)
            {
                ErrorDataRecieved(sender, args);
            }
        }

        public string Create()
        {
            IInteropResponse<string> response = Invoke<string>(
                null, 
                "Create", 
                null
            );
            if(response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return response.GetResponse();
            }
        }
        public void CreateIndexSubset(double lat, double lng, double radius)
        {
            IInteropResponse<object> response = Invoke<object>(
                null, 
                "CreateIndexSubset", 
                new object[] { lat, lng, radius }
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return;
            }
        }
        public void DestroyIndexSubset()
        {
            IInteropResponse<object> response = Invoke<object>(
                null, 
                "DestroyIndexSubset", 
                null
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return;
            }
        }
        public string Find(object index)
        {
            IInteropResponse<string> response = Invoke<string>(
                null, 
                "Find", 
                new object[] { index }
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return response.GetResponse();
            }
        }
        public string FindClosest(double lat, double lng)
        {
            IInteropResponse<string> response = Invoke<string>(
                null,
                "FindClosest",
                new object[] { lat, lng }
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return response.GetResponse();
            }
        }

        protected T GetProperty<T>(string propertyName)
        {
            IInteropResponse<T> response = Invoke<T>(
                null,
                "GetProperty",
                new object[] { propertyName }
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return response.GetResponse();
            }
        }
        protected void SetProperty<T>(string propertyName, T toSet)
        {
            IInteropResponse<T> response = Invoke<T>(
                null,
                "SetProperty",
                new object[] { propertyName, toSet }
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return;
            }
        }

        protected T GetSetting<T>(string settingName)
        {
            IInteropResponse<T> response = Invoke<T>(
                null,
                "GetSetting",
                new object[] { settingName }
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return response.GetResponse();
            }
        }
        protected void SetSetting<T>(string settingName, T toSet)
        {
            IInteropResponse<T> response = Invoke<T>(
                null,
                "SetSetting",
                new object[] { settingName, toSet }
            );
            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                return;
            }
        }
        
        public int ChartType
        {
            get
            {
                return GetProperty<int>("ChartType");
            }
            set
            {
                SetProperty<int>("ChartType", value);
            }
        }
        public int Count
        {
            get
            {
                return GetProperty<int>("Count");
            }
            set
            {
                SetProperty<int>("Count", value);
            }
        }
        public string EndTime
        {
            get
            {
                return GetProperty<string>("EndTime");
            }
            set
            {
                SetProperty<string>("EndTime", value);
            }
        }
        public string ErrorMessage
        {
            get
            {
                return GetProperty<string>("ErrorMessage");
            }
            set
            {
                throw new NotImplementedException();
                
            }
        }
        public string FileName
        {
            get
            {
                return GetProperty<string>("FileName");
            }
            set
            {
                SetProperty<String>("FileName", value);                
            }
        }
        public string Lines
        {
            get
            {
                return GetProperty<string>("Lines");
            }
            set
            {
                throw new NotImplementedException();
                
            }
        }
        public string LocationName
        {
            get
            {
                return GetProperty<string>("LocationName");
            }
            set
            {
                SetProperty<String>("LocationName", value);
            }
        }
        public string StartTime
        {
            get
            {
                return GetProperty<string>("StartTime");
            }
            set
            {
                SetProperty<string>("StartTime", value);
            }
        }
        public string StationCount
        {
            get
            {
                return GetProperty<string>("StationCount");
            }
            set
            {
                SetProperty<string>("StationCount", value);
            }
        }
        public string StationHarmonicsFileName
        {
            get
            {
                return GetProperty<string>("StationHarmonicsFileName");
            }
            set
            {
                SetProperty<string>("StationHarmonicsFileName", value);
            }
        }
        public string StationIsCurrent
        {
            get
            {
                return GetProperty<string>("StationIsCurrent");
            }
            set
            {
                SetProperty<string>("StationIsCurrent", value);
            }
        }
        public string StationIsHydraulicCurrent
        {
            get
            {
                return GetProperty<string>("StationIsHydraulicCurrent");
            }
            set
            {
                SetProperty<string>("StationIsHydraulicCurrent", value);
            }
        }
        public string StationIsReferenceStation
        {
            get
            {
                return GetProperty<string>("StationIsReferenceStation");
            }
            set
            {
                SetProperty<string>("StationIsReferenceStation", value);
            }
        }
        public string StationLat
        {
            get
            {
                return GetProperty<string>("StationLat");
            }
            set
            {
                SetProperty<string>("StationLat", value);
            }
        }
        public string StationLng
        {
            get
            {
                return GetProperty<string>("StationLng");
            }
            set
            {
                SetProperty<string>("StationLng", value);
            }
        }
        public string StationNote
        {
            get
            {
                return GetProperty<string>("StationNote");
            }
            set
            {
                SetProperty<string>("StationNote", value);
            }
        }
        public string StationPedigree
        {
            get
            {
                return GetProperty<string>("StationPedigree");
            }
            set
            {
                SetProperty<string>("StationPedigree", value);
            }
        }
        public string StationReferenceStationName
        {
            get
            {
                return GetProperty<string>("StationReferenceStationName");
            }
            set
            {
                SetProperty<string>("StationReferenceStationName", value);
            }
        }
        public string StationTimeZone
        {
            get
            {
                return GetProperty<string>("StationTimeZone");
            }
            set
            {
                SetProperty<string>("StationTimeZone", value);
            }
        }
        public string StationUnitOfMeasure
        {
            get
            {
                return GetProperty<string>("StationUnitOfMeasure");
            }
            set
            {
                SetProperty<string>("StationUnitOfMeasure", value);
            }
        }
        
        public bool IsConnected()
        {
            IInteropResponse<string> response = Invoke<string>(
                null, 
                "Test", 
                null
            );

            if (response.IsError())
            {
                throw response.GetException();
            }
            else
            {
                if (response.GetResponse().ToUpper() == "DISCONNECTED")
                {
                    return false;
                }
                else if (response.GetResponse().ToUpper() == "CONNECTED")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public ConnectionStatus GetConnectionStatus()
        {
            IInteropResponse<string> response = Invoke<string>(
                null, 
                "Test", 
                null
            );

            if (response.IsError())
            {
                return ConnectionStatus.NotConnected;
            }
            else
            {
                if (response.GetResponse().ToUpper() == "DISCONNECTED")
                {
                    return ConnectionStatus.Disconnected;
                }
                else if (response.GetResponse().ToUpper() == "CONNECTED")
                {
                    return ConnectionStatus.Connected;
                }
                else
                {
                    return ConnectionStatus.NotConnected;
                }
            }
        }
        public MDRTideEngineSettings Settings
        {
            get
            {
                return _settings;
            }
        }

        public class MDRTideEngineSettings
        {
            MDRTideEngineInteropClient _parent = null;
            public MDRTideEngineSettings(MDRTideEngineInteropClient parent)
            {
                _parent = parent;
            }
            public double RawDataStep
            {
                get
                {
                    return _parent.GetSetting<double>("RawDataStep");
                }
                set
                {
                    _parent.SetSetting<double>("RawDataStep", value);
                }
            }
            public string DateFormat
            {
                get
                {
                    return _parent.GetSetting<string>("DateFormat");
                }
                set
                {
                    _parent.SetSetting<string>("DateFormat", value);
                }
            }
        }


        public enum ConnectionStatus
        {
            Connected,
            Disconnected,
            NotConnected
        }
    }
}
