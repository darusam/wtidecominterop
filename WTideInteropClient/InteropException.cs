﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WTideInteropClient
{
    public interface IInteropResponse
    {
        bool IsError();

        Exception GetException();
        
        T GetResponse<T>();
    }
    public interface IInteropResponse<T> : IInteropResponse
    {
        T GetResponse();
    }


    [Serializable]
    public class InteropException : Exception, IInteropResponse
    {
        #region Properties
        
        

        #endregion

        /// <summary>
        /// Creates a new instance of <see cref="LegacyWrapperException"/>.
        /// </summary>
        public InteropException()
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="LegacyWrapperException"/> with the specified error message.
        /// </summary>
        /// <param name="message">Error message.</param>
        public InteropException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="LegacyWrapperException"/> with the specified error message and inner exception.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="innerException">Inner exception.</param>
        public InteropException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="LegacyWrapperException"/> with the specified serialization info and streaming context.
        /// </summary>
        /// <param name="info">Serialization info.</param>
        /// <param name="context">Streaming context.</param>
        protected InteropException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    
        

        public bool IsError()
        {
 	        return true;
        }

        public T GetResponse<T>()
        {
 	        throw new NotSupportedException();
        }

        public Exception GetException()
        {
            return this;
        }
    }

    [Serializable]
    public class InteropException<T> : InteropException, IInteropResponse<T>
    {
        public T GetResponse()
        {
            throw new NotSupportedException();
        }
    }

    [Serializable]
    public class InteropResponse<T> : IInteropResponse<T>
    {
        #region Members

        T Response;

        #endregion

        public InteropResponse(T response)
        {
            Response = response;
        }

        public T GetResponse()
        {
            return Response;
        }

        public bool IsError()
        {
            return false;
        }

        public Tout GetResponse<Tout>()
        {
            Type type = typeof(Tout);
            return (Tout) Convert.ChangeType(Response, type);
        }

        public Exception GetException()
        {
            return null;
        }
    }
}
