﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WTideInteropClient;
using MDRTELib;
using System.Threading;

namespace WTideInteropTester
{
    public class MDRTideEngineInteropTest
    {
        public static void Main(string[] args)
        {
            testNew();
            //testCOM();
            /*
            using(MDRTideEngineInteropClient client = new MDRTideEngineInteropClient())
            {
                client.ChartType = 7;
                string stationName = client.Find("Paris Road Bridge, Mississippi River Delta, Louisiana");
                DateTime startTime = new DateTime(2029, 8, 23, 8, 24, 10);
                DateTime endTime = startTime.AddHours(69);
                client.StartTime = startTime.ToString("yyyy-MM-dd hh:mm");
                client.EndTime = endTime.ToString("yyyy-MM-dd hh:mm");
                client.Settings.RawDataStep = 1800.0;
                client.LocationName = stationName;
                string stationInfo = client.Create();
                Console.WriteLine(stationInfo);
            }
            */
            

        }

        private static void testNew()
        {
            using(MDRTideEngineInteropClient client = new MDRTideEngineInteropClient())
            {
                MDRTideEngineInteropClient.ConnectionStatus status = client.GetConnectionStatus();
                client.ChartType = 7;
                string stationName = client.Find("Paris Road Bridge, Mississippi River Delta, Louisiana");
                DateTime startTime = new DateTime(2029, 8, 23, 8, 24, 10);
                DateTime endTime = startTime.AddHours(69);
                client.StartTime = startTime.ToString("yyyy-MM-dd hh:mm");
                client.EndTime = endTime.ToString("yyyy-MM-dd hh:mm");
                client.Settings.RawDataStep = 1800.0;
                client.LocationName = stationName;

                string stationInfo = client.Create();

                Console.WriteLine("-------------------------------");
                Console.WriteLine(stationInfo);
                Console.WriteLine("-------------------------------");

                stationName = client.Find("Anchorage, Alaska");
                client.LocationName = stationName;
                stationInfo = client.Create();

                Console.WriteLine("-------------------------------");
                Console.WriteLine(stationInfo);
                Console.WriteLine("-------------------------------");
                bool doContinue = true;
                while (doContinue)
                {
                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    switch(keyInfo.Key)
                    {
                        case ConsoleKey.Enter:
                            stationInfo = client.Create();
                            Console.WriteLine("-------------------------------");
                            Console.WriteLine(stationInfo);
                            Console.WriteLine("-------------------------------");
                            break;
                        case ConsoleKey.X:
                            doContinue = false;
                            break;
                        case ConsoleKey.Tab:
                            WTideInteropClient.MDRTideEngineInteropClient.ConnectionStatus st = client.GetConnectionStatus();
                            Console.WriteLine(Enum.GetName(typeof(WTideInteropClient.MDRTideEngineInteropClient.ConnectionStatus), st));
                            break;
                        default:
                            continue;

                    }
                }
            }
        }

        private static void testCOM()
        {
            ManualResetEvent evt = new ManualResetEvent(false);
            TideEngine engine;

            string stationName = "Paris Road Bridge, Mississippi River Delta, Louisiana";
            DateTime startTime = new DateTime(2029, 8, 23, 8, 24, 10);
            DateTime endTime = startTime.AddHours(69);

            object lat, lng;
            try
            {
                using(var client = new LegacyWrapperClient.Client.WrapperClient())
                {
                    Type engineType = Type.GetTypeFromProgID("MDRTE.TideEngine");
                    //client.Invoke
                    object engineInst = Activator.CreateInstance(engineType);
                    engine = (MDRTELib.TideEngine)engineInst;
                    //engine = Type.GetTypeFromProgID("MDRTE.TideEngine");
                    engine.Open();
                    engine.ChartType = 7;
                    engine.StartTime = startTime.ToString("yyyy-MM-dd hh:mm");
                    engine.EndTime = endTime.ToString("yyyy-MM-dd hh:mm");
                    SetSetting(engine, "RawDataStep", 1800.0);
                    stationName = engine.Find(stationName, out lat, out lng);
                    engine.set_LocationName(stationName);
                    object stationData = engine.Create();
                    string stationInfo = stationData as string;

                    stationName = engine.Find("Anchorage, Alaska", out lat, out lng);
                    engine.set_LocationName(stationName);
                    stationData = engine.Create();
                    stationInfo = stationData as string;
                
                    engine = null;
                }
            }
            catch (Exception e)
            {
                bool isFirst = true;
                do
                {
                    if(!isFirst)
                    {
                        Debug.WriteLine("INNER EXCEPTION:");
                    }
                    else
                    {
                        isFirst = false;
                    }
                    Debug.WriteLine(e.Message);
                    Debug.WriteLine(e.StackTrace);
                    e = e.InnerException;
                } while (e != null);
                
            }


        }

        private static void SetSetting(TideEngine te, string settingName, object value)
        {
            switch (settingName)
            {
                case "DateFormat":
                    te.Settings.DateFormat = value;
                    break;
                case "RawDataStep":
                    te.Settings.RawDataStep = value;
                    break;
                case "TimeFormat":
                    te.Settings.TimeFormat = value;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private static void dontRun()
        {
            ProcessStartInfo pStart = new ProcessStartInfo("WTideInteropServer.exe", "MDRTPipe1");
            pStart.CreateNoWindow = true;
            Process p = new Process();
            p.StartInfo = pStart;
            p.Start();
            using (NamedPipeClientStream client = new NamedPipeClientStream(".", "MDRTPipe1", PipeDirection.InOut))
            {
                client.Connect();
                using (StreamWriter sw = new StreamWriter(client))
                {
                    using (StreamReader sr = new StreamReader(client))
                    {
                        //sw.AutoFlush = true;
                        sw.WriteLine("SetProperty#LocationName#Anchorage, Alaska");
                        sw.Flush();
                        string isSuccessString = String.Empty;
                        string currentReadValue = sr.ReadLine();
                        while (currentReadValue != null && currentReadValue != "---ENDOFOUTPUT---")
                        {
                            isSuccessString = currentReadValue;
                            currentReadValue = sr.ReadLine();
                        }
                        sw.WriteLine("Create");
                        sw.Flush();
                        string value = string.Empty;
                        currentReadValue = sr.ReadLine();
                        while (currentReadValue != null && currentReadValue != "---ENDOFOUTPUT---")
                        {
                            value += currentReadValue;
                            currentReadValue = sr.ReadLine();
                        }

                        Console.ReadLine();
                        sw.WriteLine("End");
                        sw.Flush();
                        currentReadValue = sr.ReadLine();
                        while (currentReadValue != null && currentReadValue != "---ENDOFOUTPUT---")
                        {
                            currentReadValue = sr.ReadLine();
                        }
                    }
                }
            }

            p.Close();
            p = null;
        }
    }
}
